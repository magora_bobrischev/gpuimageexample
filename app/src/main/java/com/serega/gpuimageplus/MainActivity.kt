package com.serega.gpuimageplus

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Rect
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.withContext
import org.wysaid.nativePort.CGENativeLibrary
import java.nio.charset.Charset
import java.security.MessageDigest


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyclerView.setHasFixedSize(true)
        recyclerView.addItemDecoration(object : RecyclerView.ItemDecoration() {

            override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                super.getItemOffsets(outRect, view, parent, state)
                outRect.left = 10
                outRect.right = 10
            }
        })


        recyclerView.adapter = RvAdapter(
                listOf(
                        "@adjust hsl 0.02 -0.11 -0.17",
                        "@adjust hsl 0.05 -0.21 -0.27",
                        "@adjust hsl 0.08 -0.31 -0.37",
                        "@adjust hsl 0.11 -0.41 -0.47",
                        "@adjust hsl 0.14 -0.51 -0.57",
                        "@adjust hsl 0.17 -0.61 -0.67",
                        "@adjust hsl 0.20 -0.71 -0.77"
                )
        ) { effect ->
            launch(UI) {
                val bitmap = withContext(CommonPool) {
                    val src = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(resources, R.drawable.lena), (targetImage.measuredWidth * 2) / 3, (targetImage.measuredHeight * 2) / 3, true)
                    CGENativeLibrary.filterImage_MultipleEffects(src, effect, 1.0f)
                }
                targetImage.setImageBitmap(bitmap)
            }
        }
    }

    class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(effect: String) {
            Glide.with(itemView.context).clear(itemView)
            GlideApp.with(itemView.context)
                    .load(R.drawable.lena)
                    .transform(GlideTransformation(effect))
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .override(300, 300)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(itemView as ImageView)
        }
    }

    class RvAdapter(private val effects: List<String>, private val onClick: (String) -> Unit) : RecyclerView.Adapter<MainActivity.Holder>() {

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): MainActivity.Holder {
            val size = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 150f, Resources.getSystem().displayMetrics).toInt()
            val imageView = ImageView(parent.context).apply {
                layoutParams = RecyclerView.LayoutParams(size, size)
            }
            return MainActivity.Holder(imageView).apply {
                itemView.setOnClickListener {
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        onClick(effects[adapterPosition])
                    }
                }
            }
        }

        override fun onBindViewHolder(holder: MainActivity.Holder, position: Int) {
            holder.bind(effects[position])
        }

        override fun getItemCount() = effects.size
    }

    private class GlideTransformation(private val effect: String) : BitmapTransformation() {
        override fun updateDiskCacheKey(messageDigest: MessageDigest) {
            messageDigest.update(effect.toByteArray(Charset.forName("UTF-8")))
        }

        override fun transform(pool: BitmapPool, toTransform: Bitmap, outWidth: Int, outHeight: Int): Bitmap =
                CGENativeLibrary.filterImage_MultipleEffects(toTransform, effect, 1.0f)

        override fun equals(other: Any?): Boolean = effect == other

        override fun hashCode(): Int = effect.hashCode()
    }
}
