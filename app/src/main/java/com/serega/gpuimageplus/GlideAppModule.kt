package com.serega.gpuimageplus

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 10.07.18.
 */
@GlideModule
class GlideAppModule : AppGlideModule() {
}